<?php

/**
 * 基本方法类
 */
class Base
{

	public $fileField; //文件域名
    public $file; //文件上传对象
    public $base64; //文件上传对象
    public $config; //配置信息
    public $oriName; //原始文件名
    public $fileName; //新文件名
    public $fullName; //完整文件名,即从当前配置目录开始的URL
    public $filePath; //完整文件名,即从当前配置目录开始的URL
    public $fileSize; //文件大小
    public $fileType; //文件类型
    public $stateInfo; //上传状态信息,
    public $stateMap = array( //上传状态映射表，国际化用户需考虑此处数据的国际化
        "SUCCESS", //上传成功标记，在UEditor中内不可改变，否则flash判断会出错
        "文件大小超出 upload_max_filesize 限制",
        "文件大小超出 MAX_FILE_SIZE 限制",
        "文件未被完整上传",
        "没有文件被上传",
        "上传文件为空",
        "ERROR_TMP_FILE" => "临时文件错误",
        "ERROR_TMP_FILE_NOT_FOUND" => "找不到临时文件",
        "ERROR_SIZE_EXCEED" => "文件大小超出网站限制",
        "ERROR_TYPE_NOT_ALLOWED" => "文件类型不允许",
        "ERROR_CREATE_DIR" => "目录创建失败",
        "ERROR_DIR_NOT_WRITEABLE" => "目录没有写权限",
        "ERROR_FILE_MOVE" => "文件保存时出错",
        "ERROR_FILE_NOT_FOUND" => "找不到上传文件",
        "ERROR_WRITE_CONTENT" => "写入文件内容错误",
        "ERROR_UNKNOWN" => "未知错误",
        "ERROR_DEAD_LINK" => "链接不可用",
        "ERROR_HTTP_LINK" => "链接不是http链接",
        "ERROR_HTTP_CONTENTTYPE" => "链接contentType不正确",
        "INVALID_URL" => "非法 URL",
        "INVALID_IP" => "非法 IP"
    );

    /**
     * @return mixed
     * 获取七牛云的上传配置
     */
	public function getQiniuConfig()
    {
        return require('qiniu.config.php');
    }

    /**
     * 上传错误检查
     * @param $errCode
     * @return string
     */
    public function getStateInfo($errCode)
    {
        return !$this->stateMap[$errCode] ? $this->stateMap["ERROR_UNKNOWN"] : $this->stateMap[$errCode];
    }

    /**
     * 获取文件扩展名
     * @return string
     */
    public function getFileExt()
    {
        return strtolower(strrchr($this->oriName, '.'));
    }

    /**
     * 重命名文件
     * @return string
     */
    public function getFullName()
    {
        $name = strtolower(strchr($this->oriName, '.'));
        $name = time() . $name;

        $ext = $this->getFileExt();
        return sha1($name) . $ext;
    }

    /**
     * 获取文件名
     * @return string
     */
    public function getFileName () {
        return substr($this->filePath, strrpos($this->filePath, '/') + 1);
    }

    /**
     * 获取文件完整路径
     * @return string
     */
    public function getFilePath()
    {
        $rootPath = $this->qnConfig['host'] ? $this->qnConfig['host'] : $_SERVER['DOCUMENT_ROOT'];

        return $rootPath;
    }

    /**
     * 文件类型检测
     * @return bool
     */
    public function checkType()
    {
        return in_array($this->getFileExt(), $this->config["allowFiles"]);
    }

    /**
     * 文件大小检测
     * @return bool
     */
    public function  checkSize()
    {
        return $this->fileSize <= ($this->config["maxSize"]);
    }

    /**
     * 获取当前上传成功文件的各项信息
     * @return array
     */
    public function getFileInfo()
    {
        return array(
            "state" => $this->stateInfo,
            "url" => $this->filePath.'/'.$this->fullName,
            "title" => $this->fullName,
            "original" => $this->oriName,
            "type" => $this->fileType,
            "size" => $this->fileSize
        );
    }
}