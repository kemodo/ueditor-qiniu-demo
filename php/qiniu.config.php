<?php
 
return [

	/* 七牛云存储信息配置 */
	'bucket'      => 'kstone', // 七牛存储空间的名称
	'host'        => '',    //外链默认域名
	'access_key'  => '',    //七牛云秘钥AK
	'secret_key'  => '',    //七牛云SK

];